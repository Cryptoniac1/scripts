#!/bin/bash
# This is a script to easily make use of my scanner, a brother DS-640, to quickly scan and concatenate pdfs into one file
# Run scanimage -L at least once and change the grep statement to whatever makes sense for your scanner
# Dependencies are sane and pdftk

TMPDIR=`mktemp -d tmp.XXXXXXXX`
scanner=$(scanimage -L | grep brother | awk '{print $2}' | sed "s/\`//g" | sed "s/'//g")
i=1
while [ "$finished" != "q" ]
do
	num=$(printf "%02d" $i)
	scanimage -d "$scanner" --AutoDocumentSize=yes --resolution 100 -o $TMPDIR/$num.pdf
	((i=i+1))
	read -p "If this was the last page press q and then enter, otherwise insert the next page then hit enter " finished
done
read -p "Pick a file name, without the pdf extension " filename
pdftk $TMPDIR/*.pdf cat output "$filename.pdf"
while true
do
    read -p "Do you wish to encrypt this file? y/n: " yn
    case $yn in
        [Yy]* ) gpg -r lucas@carrcrypt.com -e "$filename.pdf"; shred "$filename.pdf"; rm "$filename.pdf"; break;;
        [Nn]* ) break;;
        * ) echo "Please answer yes or no.";;
    esac
done
rm -rf $TMPDIR

#!/bin/bash
# Script that gives the word count of what is in the clipboard
# To be run as a one time script

XDG_RUNTIME_DIR=/run/user/$(id -u) notify-send -a "Word Count" "$(wl-paste --primary | wc -w)"


# To be run as a daemon
#currentcb=$(wl-paste --primary)
#oldcb="$currentcb"
#echo $currentcb | wc -w
#while [ "$oldcb" = "$currentcb" ] 
#do 
#currentcb=$(wl-paste --primary)
#	if [ "$oldcb" != "$currentcb" ]  
#	then
#		oldcb=currentcb
#		XDG_RUNTIME_DIR=/run/user/$(id -u) notify-send -a "Word Count" "$(echo "$currentcb" | wc -w)"
#	fi
#oldcb=$currentcb
#done

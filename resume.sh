# Use in conjunction with suspend.sh
# Resume a focused window with the -CONT option
#!/bin/bash
wpid="$(xdotool getactivewindow getwindowpid)"
kill -CONT "$wpid"
echo "$wpid"

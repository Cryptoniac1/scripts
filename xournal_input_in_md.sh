#!/bin/bash

# Script meant for use with markdown files in vim. If given input as an
# argument it will treat that as the file name. Else it will make a random one
# based on the date
# To use with vim add this to your vimrc (changing the keybindings as you see fit):
# 
# > "insert xournalpp page in md format
# > nnoremap ,i :r !~/scripts/xournal_input_in_md.sh<CR>
# > "edit xournalpp page 
# > nnoremap ,e :execute ":!~/scripts/xournal_input_in_md.sh " . shellescape( '<cfile>' )<CR>
# > "edit copy page and edit the copy
# > nnoremap ,c :execute ":r!~/scripts/xournal_input_in_md.sh -c " . shellescape( '<cfile>' )<CR>

case $1 in
	-c)
	input="$@"
	name=${input%.*}
	name1=$(echo "$name" | sed 's/\-c //g' | sed 's/human_input\///g')
	date=$(date +%s)
	name=$(echo "$name1.$date")
	cp human_input/$name1.xopp human_input/$name.xopp;;
	*)
	if [ ! "$@" ]
	then
		name=$(date | md5sum | awk '{print $1}')
	else
		input="$@"
		name=${input%.*}
		name=$(echo $name | sed 's/human_input//g')
		skip="yes"
		echo $name
		echo $skip
	fi;;
esac


mkdir human_input 2>/dev/null # make the directory for the files to be stored in (if applicable)

xournalpp human_input/$name.xopp 2>/dev/null
xournalpp human_input/$name.xopp -p human_input/$name.pdf 2>/dev/null

if [ "$skip" == "yes" ]
then
	exit 0
else
	echo "![](human_input/$name.pdf)\\"
fi

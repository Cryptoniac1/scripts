#!/bin/bash
# A script to launch a terminal with tmux by default the options will be new session or attach

OUTPUT=/tmp/session_tmp.txt
env WAYLAND_DISPLAY=$(echo WAYLAND_DISPLAY) # Make sure it is set to open as a wayland session

dialog --radiolist "New or Attach:" 0 0 0 \
  1 "New" on \
  2 "Attach" Off \
  3 "None" Off 2> $OUTPUT
if [ $(grep -i "1" $OUTPUT)  ]
then
  echo "New"
  tmux 
elif [ $(grep -i "2" $OUTPUT) ]
then
  echo "Attach"
  session=$(tmux list-sessions | awk '{print $1}' | sed 's/://g' | fzf)
  tmux a -t $session
else
  zsh
fi
rm $OUTPUT

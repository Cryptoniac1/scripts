#add this to your vimrc or bashrc to quickly open compiled groff ms documents
#!/bin/bash
doc=$1
echo $doc
if [[ $doc == *.ms ]]
then
name=$(echo $doc | sed 's/\.ms//')
echo $name
name=$name.pdf
echo $name;
zathura "$name"
fi
if [[ $doc == *.md ]]
then
name=$(echo $doc | sed 's/.md//')
echo $name
name=$name.pdf
echo $name;
zathura "$name"
fi
if [[ $doc == *.tex ]]
then
name=$(echo $doc | sed 's/.tex//')
echo $name
name=$name.pdf
echo $name;
zathura "$name"
fi

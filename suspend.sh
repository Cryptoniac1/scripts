# Use in conjunction with resume.sh
# Pause any currently running process that is focused
#!/bin/bash
wpid="$(xdotool getactivewindow getwindowpid)"
kill -STOP "$wpid"
echo "$wpid"

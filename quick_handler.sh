#!/bin/bash
#Script to quickly open files for use in wm's

thingy="$(wl-paste | sed "s/~/$(echo $HOME | sed 's/\//\\\//g')/g")" # replace any instance of ~ with the user directory
result=$(echo $thingy | grep -e youtube -e yt -e youtu.be)
echo $result
if [ -z "$result" ]
then
echo "1"
echo $thingy
xdg-open "$thingy"
else
echo "2"
mpv --save-position-on-quit $thingy
fi

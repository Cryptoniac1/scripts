#!/bin/bash
# A script to pass a torrent file into qBittorrent-cli via newsboat
URL=$1
TMPFILE=`mktemp /tmp/qBT-passthrough.bashXXXXXXXX`
TORRENTFILE=`mktemp /tmp/qBT-passthrough.bashXXXXXXXX`
echo "$HOME/Downloads/Torrent/" > ${TMPFILE}
echo "Please Edit The Location"
read
$EDITOR ${TMPFILE}
location=$(cat ${TMPFILE})
mkdir -p "$(cat ${TMPFILE})"
echo $location
wget "$URL" -O "${TORRENTFILE}"
echo $TORRENTFILE
echo $location
read
qbt torrent add file "$TORRENTFILE" -f "$location"
rm $TMPFILE $TORRENTFILE

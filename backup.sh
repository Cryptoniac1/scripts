##A script for backing up all dotfiles
## to ignore a folder add a file named tagfile in the containing directory
#!/bin/bash
dir=~/Backups
export DISPLAY=:0 &&
XDG_RUNTIME_DIR=/run/user/$(id -u) notify-send -a "Backup" "Backup is now running";
mkdir -p "$dir/$(date "+Bak_%y-%m-%d")";
tar -I "pixz -9" -cf "$dir/$(date "+Bak_%y-%m-%d")/dotbackups.tar.xz" --exclude-tag=tagfile ~/.[!.]?*;
pacman -Qqe | grep -v "$(pacman -Qqm)" > $dir/$(date "+Bak_%y-%m-%d")/pacman.lst
XDG_RUNTIME_DIR=/run/user/$(id -u) notify-send -a "Backup" "Backup is Finished"

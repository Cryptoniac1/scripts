#!/bin/bash
if [ -f /usr/bin/acpi ]
then
img=/usr/share/icons/breeze-dark/status/16/battery
bat=$(acpi)
echo $bat
if [ $(echo $bat | awk '{ print $3 }') = Full, ]
	then
	state=full
elif [ $(echo $bat | awk '{ print $3 }') = Discharging, ]
	then
	state=discharging
	percent=$(echo $bat | awk '{ print $4 }' | sed 's/%,//')
elif [ $(echo $bat | awk '{ print $3 }') = Charging, ]
	then
	state=charging
	percent=$(echo $bat | awk '{ print $4 }' | sed 's/%,//')
	fi
echo $state
	if [ "$state" == "full" ]
	then
	echo "full"
	icon="$img-$state.svg"
	elif [ "$state" == "charging" ]
		then
		if [ $percent -ge 90 ]  && [ $percent -lt 100 ]
		then
		echo "90"
		icon="$img-090-$state.svg"
		elif [ $percent -ge 80 ]  && [ $percent -lt 90 ]
		then
		echo "80+"
		icon="$img-080-$state.svg"
		elif [ $percent -ge 70 ]  && [ $percent -lt 80 ]
		then
		echo "70+"
		icon="$img-070-$state.svg"
		elif [ $percent -ge 60 ]  && [ $percent -lt 70 ]
		then
		echo "60+"
		icon="$img-060-$state.svg"
		elif [ $percent -ge 50 ]  && [ $percent -lt 60 ]
		then
		echo "50+"
		icon="$img-050-$state.svg"
		elif [ $percent -ge 40 ]  && [ $percent -lt 50 ]
		then
		echo "40+"
		icon="$img-040-$state.svg"
		elif [ $percent -ge 30 ]  && [ $percent -lt 40 ]
		then
		echo "30+"
		icon="$img-030-$state.svg"
		elif [ $percent -ge 20 ]  && [ $percent -lt 30 ]
		then
		echo "20+"
		icon="$img-020-$state.svg"
		elif [ $percent -ge 10 ]  && [ $percent -lt 20 ]
		then
		echo "10+"
		icon="$img-010-$state.svg"
		elif [ $percent -le 9 ]  && [ $percent -lt 10 ]
		then
		echo "9+"
		icon="$img-000-$state.svg"
		fi
	elif [ "$state" == "discharging" ]
		then
		if [ $percent -ge 90 ]  && [ $percent -lt 100 ]
		then
		echo "90+"
		icon="$img-090.svg"
		elif [ $percent -ge 80 ]  && [ $percent -lt 90 ]
		then
		echo "80+"
		icon="$img-080.svg"
		elif [ $percent -ge 70 ]  && [ $percent -lt 80 ]
		then
		echo "70+"
		icon="$img-070.svg"
		elif [ $percent -ge 60 ]  && [ $percent -lt 70 ]
		then
		echo "60+"
		icon="$img-060.svg"
		elif [ $percent -ge 50 ]  && [ $percent -lt 60 ]
		then
		echo "50+"
		icon="$img-050.svg"
		elif [ $percent -ge 40 ]  && [ $percent -lt 50 ]
		then
		echo "40+"
		icon="$img-040.svg"
		elif [ $percent -ge 30 ]  && [ $percent -lt 40 ]
		then
		echo "30+"
		icon="$img-030.svg"
		elif [ $percent -ge 20 ]  && [ $percent -lt 30 ]
		then
		echo "20+"
		icon="$img-020.svg"
		elif [ $percent -ge 10 ]  && [ $percent -lt 20 ]
		then
		echo "10+"
		icon="$img-010.svg"
		elif [ $percent -le 9 ]  && [ $percent -lt 10 ]
		then
		echo "9+"
		icon="$img-000.svg"
	fi
fi
echo "$icon"
notify-send  -a "Time" -i "$icon" "$(date "+ %c") $(acpi)"
fi
if [ ! -f /usr/bin/acpi ]
then
notify-send  -a "Time" -i /usr/share/icons/breeze-dark/actions/22/clock-large.svg "$(date "+ %c")"
fi

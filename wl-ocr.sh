#!/bin/bash
# a script to quickly ocr something on screen
TMPDIR=`mktemp -d tmp.XXXXXXXX`
case "$1" in
-n) 
# default behaviour
/usr/share/sway-contrib/grimshot save area $TMPDIR/file.png
tesseract $TMPDIR/file.png $TMPDIR/output
cat $TMPDIR/output.txt | wl-copy 
rm -rf $TMPDIR;;

-j) 
# make use of gazou for japanese/chinese characters
/usr/share/sway-contrib/grimshot save area $TMPDIR/file.png
gazou $TMPDIR/file.png | sed '/^[[:space:]]*$/d' | wl-copy
rm -rf $TMPDIR;;
esac

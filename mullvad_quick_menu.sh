#!/bin/bash
# This script relies on mullvad vpn being installed
# There should be a file in ~/.local/ called mullvad_countries
# The file should have the following format
# Country country_code
# city country_code city_code

countrySelect(){
	Country=$(cat ~/.local/mullvad_countries | awk '{print $1}' |  bemenu -i --tf '#D13900' --ff '#FFFFFF' --hf '#D13900' --sf '#FFFFFF' --nf '#FFFFFF' --cf '#FFFFFF' --af '#FFFFFF' --sb --nb gray -p "Select a location:")
	code=$(grep "$Country" ~/.local/mullvad_countries | awk '{$1=""; print substr($0,2)}')
	echo $code
	mullvad relay set location $code
}

TASK=$(echo -e "Relocate\nReconnect\nDisconnect" |  bemenu -i --tf '#D13900' --ff '#FFFFFF' --hf '#D13900' --sf '#FFFFFF' --nf '#FFFFFF' --cf '#FFFFFF' --af '#FFFFFF' --sb --nb gray -p "What would you like to do:")

if [ $(echo $TASK) = Relocate ]
	then
	countrySelect
elif [ $(echo $TASK) = Reconnect ]
	then
	mullvad connect # this is just in case it was previously disconnected
	mullvad reconnect
elif [ $(echo $TASK) = Disconnect ]
	then
	mullvad disconnect
fi


#!/bin/bash
# create a new wallpaper for the lockscreen and reset i3lock as it updates
#Change wallpapers= to the directory you want
wallpapers=~/Nextcloud/Media/wallpapers
while [ -n "$1" ]; do
case "$1" in
-r) wallpaper=$wallpapers/$(ls $wallpapers | shuf | head -1); echo $wallpaper;;

-p) wallpaper=$wallpapers/$(ls ~/Nextcloud/Media/wallpapers/ | awk '{ print $1 }' | sed 's/://g' |  bemenu -i --tf '#D13900' --ff '#FFFFFF' --hf '#D13900' --sf '#FFFFFF' --nf '#FFFFFF' --cf '#FFFFFF' --af '#FFFFFF' --sb --nb gray -l 20 -p 'Image: ');;
esac
shift
done

if [ -v $(pgrep sway) ] # check if I'm running i3 or sway
then
if [ ${wallpaper: -4} == ".png" ]
then
cp $wallpaper $wallpapers/lock/in.png
nitrogen --set-tiled $wallpapers/lock/in.png
elif [ ${wallpaper: -4} == ".jpg" ]
then 
convert $wallpaper $wallpapers/lock/in.png
nitrogen --set-tiled $wallpapers/lock/in.png
fi
elif [ -v $(pgrep i3) ]
then
killall swaybg
if [ ${wallpaper: -4} == ".png" ]
then
cp $wallpaper $wallpapers/lock/in.png
swaybg -i $wallpapers/lock/in.png&
elif [ ${wallpaper: -4} == ".jpg" ]
then 
convert $wallpaper $wallpapers/lock/in.png
swaybg -i $wallpapers/lock/in.png&
fi
fi

if [ $(pgrep hyprpaper) ] 
then 
pkill hyprpaper
hyprpaper
fi

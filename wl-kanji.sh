#!/bin/bash
# a script to quickly ocr something on screen
TMPDIR=`mktemp -d /tmp/tmp.XXXXXXXX`
/usr/share/sway-contrib/grimshot save area $TMPDIR/file.png
# make use of gazou for japanese/chinese characters
text=$(gazou $TMPDIR/file.png | tr -d '\n' | sed 's/://g' | sed 's/\.//g' | sed 's/ //g' )
XDG_RUNTIME_DIR=/run/user/$(id -u) notify-send -a "Kanji" "$(sdcv -n $text)"
rm -rf $TMPDIR

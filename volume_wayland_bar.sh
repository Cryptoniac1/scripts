#!/bin/bash
case $@ in 
	-u) 
pactl set-sink-volume @DEFAULT_SINK@ +10% 
pactl get-sink-volume @DEFAULT_SINK@ | awk '{print $5}' | sed 's/%//g' | bc > $XDG_RUNTIME_DIR/wob.sock;;
	-d)
pactl set-sink-volume @DEFAULT_SINK@ -10% 
pactl get-sink-volume @DEFAULT_SINK@ | awk '{print $5}' | sed 's/%//g' | bc > $XDG_RUNTIME_DIR/wob.sock;;
esac

# Quick playlist picker (and basic commands) for mpd
# makes use of bemenu and mpc
#!/bin/bash
action=$(echo -e "Action\nPlaylist\nPlayerctl" | bemenu -i --tf '#D13900' --ff '#FFFFFF' --hf '#D13900' --sf '#FFFFFF' --nf '#FFFFFF' --cf '#FFFFFF' --af '#FFFFFF' --sb --nb gray -p 'What would you like to do: ')
if [ -z "$(echo $action)"  ] # Check if <ESC> was pressed
	then
	echo "Yes"
	exit 1
fi
if [ $(echo $action) = Playlist ]
	then
	playlist=$(ls ~/.config/mpd/playlists |\
	sed 's/\.m3u//g' |\
	   bemenu -i --tf '#D13900' --ff '#FFFFFF' --hf '#D13900' --sf '#FFFFFF' --nf '#FFFFFF'\
	   --cf '#FFFFFF' --af '#FFFFFF' --sb --nb gray -p 'pick a playlist')
	mpc clear
	mpc load $playlist
	mpc shuffle
	mpc play
elif [ $(echo $action) = Action ] 
	then
	choice=$(echo -e "Play\nPause\nNext\nPrevious\nInfo\nAdd" | \
	   bemenu -i --tf '#D13900' --ff '#FFFFFF' --hf '#D13900' --sf '#FFFFFF' --nf '#FFFFFF'\
	   --cf '#FFFFFF' --af '#FFFFFF' --sb --nb gray -p 'Please select an action:')
	if [ $(echo $choice) = Play ] || [ $(echo $choice) = Pause ]
	then
		mpc toggle
	elif [ $(echo $choice) = Next ] 
		then
		mpc next
	elif [ $(echo $choice) = Previous ]
		then
		mpc prev
	elif [ $(echo $choice) = Info ]
		then
		notify-send "$(mpc status)"
	elif [ $(echo $choice) = Add ]
		then
		echo "You are in add"
		song=$(echo " " | \
		   bemenu -i --tf '#D13900' --ff '#FFFFFF' --hf '#D13900' --sf '#FFFFFF' --nf '#FFFFFF'\
		   --cf '#FFFFFF' --af '#FFFFFF' --sb --nb gray -p 'Please search for a song title:')
		selection=$(mpc search title "$song" | \
		   bemenu -i --tf '#D13900' --ff '#FFFFFF' --hf '#D13900' --sf '#FFFFFF' --nf '#FFFFFF'\
		   --cf '#FFFFFF' --af '#FFFFFF' --sb --nb gray -l 20 -p 'Please select a song:')
		if [ -z "$(echo $selection)"  ]
			then
			echo "Yes"
			exit 1
		fi
		mpc insert "$selection"
	fi
	
elif [ $(echo $action) = Playerctl ] 
	then
	player=$(playerctl -l | \
	   bemenu -i --tf '#D13900' --ff '#FFFFFF' --hf '#D13900' --sf '#FFFFFF' --nf '#FFFFFF'\
	   --cf '#FFFFFF' --af '#FFFFFF' --sb --nb gray -p 'Please select a player:')
	choice=$(echo -e "Play\nPause\nNext\nPrevious\nInfo" | \
	   bemenu -i --tf '#D13900' --ff '#FFFFFF' --hf '#D13900' --sf '#FFFFFF' --nf '#FFFFFF'\
	   --cf '#FFFFFF' --af '#FFFFFF' --sb --nb gray -p 'Please select an action:')
	if [ $(echo $choice) = Play ]
	then
		playerctl -p $player play
	elif [ $(echo $choice) = Pause ]
		then
		playerctl -p $player pause
	elif [ $(echo $choice) = Next ] 
		then
		playerctl -p $player  next
	elif [ $(echo $choice) = Previous ]
		then
		playerctl -p $player  prev
	fi
fi

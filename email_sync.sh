#!/bin/bash
# Sync email with mutt wizard and give notifications
# intended to be put into a crontab
# if it is not working in crontab add 
# XDG_RUNTIME_DIR=/run/user/$(id -u) 
# before executing the script
export DISPLAY=:0
notification=$(/usr/local/bin/mw -Y | grep Added)
notify-send -a Email-Sync -i /usr/share/icons/breeze-dark/actions/16/gnumeric-link-email.svg "$notification"

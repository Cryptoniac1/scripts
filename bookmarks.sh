#!/bin/bash
# A script for managing all bookmarks through bemenu 
# Depends on: wl-paste, bemenu
# Create a file for bookfile variable with two lines containing ADD REMOVE and IDK on separate lines; 
# then point bookfile to that file
# For the special functions such as remove, URL and IDK put them as a blank line on the file

bookfile=~/Nextcloud/Bookmarks/bookmarks.txt
browser=$(echo -e "xdg-open\nlibrewolf\nfirefox\nbrave\nchromium\nvanadium\nqutebrowser\nVPN\nDictionary\nYouTube" |  bemenu -i --tf '#D13900' --ff '#FFFFFF' --hf '#D13900' --sf '#FFFFFF' --nf '#FFFFFF' --cf '#FFFFFF' --af '#FFFFFF' --sb --nb gray)

if [ -z "$(echo $browser)"  ]
then
	exit 1
fi

if [ $(echo $browser) = YouTube ] # Non-Bookmark Functions
then
	~/scripts/yt-search
	exit 1
elif [ $(echo $browser) = Dictionary ]
then
	word=$(echo -e " " |  bemenu -i --tf '#D13900' --ff '#FFFFFF' --hf '#D13900' --sf '#FFFFFF' --nf '#FFFFFF' --cf '#FFFFFF' --af '#FFFFFF' --sb --nb gray -p "Enter the word: ") 
	notify-send -A "definition" "$(sdcv $word)"
	exit 1
elif [ $(echo $browser) = VPN ]
then
	~/scripts/mullvad_quick_menu.sh
	exit 1
elif [ $(echo $browser) = brave ]
then
	option=$(echo -e "private\nschool\nAmazon" |  bemenu -i --tf '#D13900' --ff '#FFFFFF' --hf '#D13900' --sf '#FFFFFF' --nf '#FFFFFF' --cf '#FFFFFF' --af '#FFFFFF' --sb --nb gray)
	browser="$browser --profile-directory=$option"
fi

tag=$(awk '{print $1}'  <  $bookfile | awk '!a[$0]++' |  bemenu -i --tf '#D13900' --ff '#FFFFFF' --hf '#D13900' --sf '#FFFFFF' --nf '#FFFFFF' --cf '#FFFFFF' --af '#FFFFFF' --sb --nb gray)
echo $tag

if [ $(echo $tag) = REMOVE ]
	then
	removal=$(awk '{print $2}' < $bookfile | sed  '/./,$!d' | sort |  bemenu -i --tf '#D13900' --ff '#FFFFFF' --hf '#D13900' --sf '#FFFFFF' --nf '#FFFFFF' --cf '#FFFFFF' --af '#FFFFFF' --sb --nb gray -p "Select a bookmark to remove:")
	if [ -z "$(echo $removal)"  ]
	then
		echo "Yes"
		exit 1
	fi

	sed -i "/$(echo $removal)/d" $bookfile

	echo |  bemenu -i --tf '#D13900' --ff '#FFFFFF' --hf '#D13900' --sf '#FFFFFF' --nf '#FFFFFF' --cf '#FFFFFF' --af '#FFFFFF' --sb --nb gray -p "Successfully Removed"

	elif [ -z "$(echo $tag)"  ]
	then
		echo "Yes"

elif [ $(echo $tag) = IDK ]
	then
	name=$(awk '{print $2}' < $bookfile | sed  '/./,$!d' |  bemenu -i --tf '#D13900' --ff '#FFFFFF' --hf '#D13900' --sf '#FFFFFF' --nf '#FFFFFF' --cf '#FFFFFF' --af '#FFFFFF' --sb --nb gray )
	if [ -z "$(echo $name)"  ]
	then
		echo "Yes"
		exit 1
	fi
elif [ $(echo $tag) = URL ]
	then
		clip=$(echo -e "NO\nYES" |  bemenu -i --tf '#D13900' --ff '#FFFFFF' --hf '#D13900' --sf '#FFFFFF' --nf '#FFFFFF' --cf '#FFFFFF' --af '#FFFFFF' --sb --nb gray -p "Is the url in the clipboard:") 
if [ $(echo $clip) = YES ]
	then
		link=$(wl-paste)
		echo $link
		$browser "$link"
		exit 1
	else
		link=$(echo |  bemenu -i --tf '#D13900' --ff '#FFFFFF' --hf '#D13900' --sf '#FFFFFF' --nf '#FFFFFF' --cf '#FFFFFF' --af '#FFFFFF' --sb --nb gray -p "Enter the URL")
	if [ -z "$(echo $link)"  ]; then exit 1; fi
		$browser "https://$link"
		exit 1
fi
elif [ $(echo $tag) = Search ]
	then
	name=$(grep "$tag" $bookfile | awk '{print $2}' |  sort |  bemenu -i --tf '#D13900' --ff '#FFFFFF' --hf '#D13900' --sf '#FFFFFF' --nf '#FFFFFF' --cf '#FFFFFF' --af '#FFFFFF' --sb --nb gray -p "Pick a Search Engine:")
	if [ -z "$(echo $name)"  ]
	then
		echo "Yes"
		exit 1
	else
		search_term=$(echo |  bemenu -i --tf '#D13900' --ff '#FFFFFF' --hf '#D13900' --sf '#FFFFFF' --nf '#FFFFFF' --cf '#FFFFFF' --af '#FFFFFF' --sb --nb gray -p "Search:")
		if [ -z "$(echo $search_term)"  ]; then exit 1; fi
		search_term=${search_term//\ /+}
		echo $search_term
		link=$(grep "$name" $bookfile | awk '{print $3}')
		link=$link$search_term
		echo $link
		$browser "$link"
		exit 1
	fi
elif [ $(echo $tag) = Translate ]
	then
		name=$(grep "$tag" $bookfile | awk '{print $2}' |  sort |  bemenu -i --tf '#D13900' --ff '#FFFFFF' --hf '#D13900' --sf '#FFFFFF' --nf '#FFFFFF' --cf '#FFFFFF' --af '#FFFFFF' --sb --nb gray -p "Pick a Language to translate to:")
		if [ -z "$(echo $name)"  ]
		then
		echo "Yes"
		exit 1
	else
		clip=$(echo -e "YES\nNO" |  bemenu -i --tf '#D13900' --ff '#FFFFFF' --hf '#D13900' --sf '#FFFFFF' --nf '#FFFFFF' --cf '#FFFFFF' --af '#FFFFFF' --sb --nb gray -p "Is the text to translate in the clipboard:") 
	if [ $(echo $clip) = YES ]
	then
		search_term=$(wl-paste)
		search_term=${search_term//\ /+}
		echo $search_term
		link=$(grep "$name" $bookfile | awk '{print $3}')
		link=$link$search_term
		echo $link
		$browser "$link"
		exit 1
	else
		search_term=$(echo |  bemenu -i --tf '#D13900' --ff '#FFFFFF' --hf '#D13900' --sf '#FFFFFF' --nf '#FFFFFF' --cf '#FFFFFF' --af '#FFFFFF' --sb --nb gray -p "Please enter what you to translate:")
		search_term=${search_term//\ /+}
		echo $search_term
		link=$(grep "$name" $bookfile | awk '{print $3}')
		link=$link$search_term
		echo $link
		$browser "$link"
		exit 1
	fi
	fi
else
	name=$(grep "$tag" $bookfile | awk '{print $2}' | sort |   bemenu -i --tf '#D13900' --ff '#FFFFFF' --hf '#D13900' --sf '#FFFFFF' --nf '#FFFFFF' --cf '#FFFFFF' --af '#FFFFFF' --sb --nb gray )
	if [ -z "$(echo $name)"  ]
	then
	echo "Yes"
	exit 1
	fi
	link=$(grep "$name" $bookfile | awk '{print $3}')
	$browser "$link"
fi

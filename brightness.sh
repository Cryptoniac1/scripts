#!/bin/bash
max='255'
current=$(cat /sys/class/backlight/amdgpu_bl1/brightness)
echo $current
case $@ in
	-p)
if [ $(echo $current) == 255 ]
then
exit 1
else
new=$(echo "$current+5" | bc)
echo $new | sudo tee /sys/class/backlight/amdgpu_bl1/brightness
echo "($new*100)/255" | bc > $XDG_RUNTIME_DIR/wob.sock
fi
;;
	-m)
if [ $(echo $current) == 5 ]
then
exit 1
else
new=$(echo "$current-5" | bc)
echo $new | sudo tee /sys/class/backlight/amdgpu_bl1/brightness
echo "($new*100)/255" | bc > $XDG_RUNTIME_DIR/wob.sock
fi
;;
esac

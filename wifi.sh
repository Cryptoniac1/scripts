# CLI command to connect to wifi
# Makes use of NetworkManager

case $@ in
	-d)
net=$(nmcli connection show | grep wlan | awk '{print $1}')
nmcli connection down $net
;;
	*)
nmcli radio wifi on
nmcli dev wifi rescan
network=$(nmcli dev wifi list | awk '{print $2}' | fzf)
if [ $(grep $network ~/.local/share/wifi)  ]
	then
	echo -e "\nconnecting to $network"
	nmcli connection up $network	
else
	nmcli --ask dev wifi connect $network
	echo $network >> ~/.local/share/wifi
fi
;;
esac

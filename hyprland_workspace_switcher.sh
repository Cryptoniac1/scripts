#!/bin/bash
# Script to quickly switch workspaces in Hyprland

int='^[0-9]+([.][0-9]+)?$'
name=$(hyprctl workspaces | grep ID | awk '{print $4}' | sed 's/(//g' | sed 's/)//g' | bemenu -i --tf '#D13900' --ff '#FFFFFF' --hf '#D13900' --sf '#FFFFFF' --nf '#FFFFFF' --cf '#FFFFFF' --af '#FFFFFF' --sb --nb gray -l 20 -p "Please choose a workspace to switch to")
if [[ $name =~ $int ]] 
then
	echo integer
	hyprctl dispatch workspace $name
elif [ $(echo $name | grep special) ]
then 
	echo special
	hyprctl dispatch workspace $name
else
	echo dynamic
	hyprctl dispatch workspace name:$name
fi

#add this to your vimrc or bashrc to quickly compile groff ms documents
#!/bin/bash
doc=$1
if [[ $doc == *.ms ]]
then
name=$(echo $doc | sed 's/\.ms//g')
name=$name.pdf
cat "$1" | groff -R -e -ms -P-pletter -Tps > qwertyuiop.ps;
ps2pdf qwertyuiop.ps  "$name"
rm qwertyuiop.ps
fi
if [[ $doc == *.md ]]
then
name=$(echo $doc | sed 's/.md//g')
name=$name
pandoc -f markdown $name.md -o $name.pdf --pdf-engine=xelatex
fi
if [[ $doc == *.tex ]]
then
pdflatex $doc
fi

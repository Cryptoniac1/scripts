#!/bin/bash
# Script to send a message to a MailDir
MAILDIR=$1
SUBJECT=$2
FROM=$3
MESSAGE=$4
echo "$MAILDIR"
echo "$SUBJECT"
echo "$FROM"
echo "$MESSAGE"
file=$(date +%s)
touch $MAILDIR/cur/$file
echo "Return-Path: <log>
X-Original-To: log
Delivered-To: log
From: $FROM
To: log
Subject: $SUBJECT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Content-Type: text/plain; charset=UTF-8
$MESSAGE" > $MAILDIR/cur/$file

#!/usr/bin/env bash

# A script to sort through a keepassxc database with bemenu and copy the attributes to the clipboard
# use requires GNU pass
# If you don't want to use pass replace the pass show command with echo "$PASSWORD_OF_DATABASE"

DB=~/Nextcloud/Documents/youknowwhatitis.kdbx # The place of the database file
key=~/Documents/key # The place of the key file

Type=$(echo -e "Password\nOTP\nUsername\nGenerate\nURL" |  bemenu -i --tf '#D13900' --ff '#FFFFFF' --hf '#D13900' --sf '#FFFFFF' --nf '#FFFFFF' --cf '#FFFFFF' --af '#FFFFFF' --sb --nb gray -p "What attribute to copy:")
	if [ -z "$(echo $Type)"  ]
	then
	echo "Yes"
	exit 1
	fi
if [ $(echo $Type) = Generate ]
then
keepassxc-cli generate | xclip -i -selection clipboard
exit 1
fi

Search=$(echo |  bemenu -i --tf '#D13900' --ff '#FFFFFF' --hf '#D13900' --sf '#FFFFFF' --nf '#FFFFFF' --cf '#FFFFFF' --af '#FFFFFF' --sb --nb gray -p "Search for what $Type:")
	if [ -z "$(echo $Search)"  ]
	then
	echo "Yes"
	exit 1
	fi
Entry=$(pass show keepass | keepassxc-cli search -k "$key" "$DB"  $Search |  bemenu -i --tf '#D13900' --ff '#FFFFFF' --hf '#D13900' --sf '#FFFFFF' --nf '#FFFFFF' --cf '#FFFFFF' --af '#FFFFFF' --sb --nb gray )
	if [ -z "$(echo $Entry)"  ]
	then
	echo "Yes"
	exit 1
	fi
case $Type in
Username)
	pass show keepass | keepassxc-cli clip -a Username -k "$key" "$DB" "$Entry"
;;
URL)
	pass show keepass | keepassxc-cli clip -a URL -k "$key" "$DB" "$Entry"
;;
Password)
	pass show keepass | keepassxc-cli clip -k "$key" "$DB" "$Entry"
;;
OTP)
	pass show keepass | keepassxc-cli clip -t -k "$key" "$DB" "$Entry"
;;
esac

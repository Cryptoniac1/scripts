#!/bin/bash
# This is a script for when I need to type something and want to use vim to type it
# it will copy the result to the clipboard
TMPFILE=`mktemp /tmp/viminsert.XXXXXXXX.md`
alacritty --class viminsert -e vim $TMPFILE
cat $TMPFILE | wl-copy

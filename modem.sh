#!/bin/bash
sudo qmicli --device-open-proxy --device=/dev/cdc-wdm0 --dms-set-fcc-authentication
mmcli -m 0 --set-allowed-modes=4G
#mmcli -m 0 --create-bearer="apn=Wholesale"
mmcli -m 0 -e
nmcli device connect cdc-wdm0
nmcli connection show

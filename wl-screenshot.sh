#!/bin/bash
#a script to quickly screen shot something and save the path in the clipboard
name=$(date "+grim_%y-%m-%d-%s")
#/usr/share/sway/scripts/grimshot save area ~/Nextcloud/Media/Screenshots/$name.png
/usr/share/sway-contrib/grimshot save area ~/Nextcloud/Media/Screenshots/$name.png
wl-copy "~/Nextcloud/Media/Screenshots/$name.png"

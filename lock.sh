# A one-liner to point to for my configs
if [ -v $(pgrep swaylock) ]
then
swaylock -i ~/Nextcloud/Media/wallpapers/lock/in.png -t -f 
else
killall -signal SIGUSR1 swaylock
fi
